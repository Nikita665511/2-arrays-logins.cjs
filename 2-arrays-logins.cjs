/*const data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]
*/

function agender(data) {
    const agender= data.filter((data) => data.gender == 'Agender');
    return agender;
}
//console.log(agender(data));


function ipv4addressofpersons(data){
    const ipv4address = data.map((ipvalue) => {
    let result = ipvalue.ip_address.split('.');
    return result.map((resultofNum) => parseInt(resultofNum));
    });
    return ipv4address;
}

//console.log(ipv4addressofpersons(data));

function sumofsecondcomponents(data) {
    const ipv4address = ipv4addressofpersons(data);
    const result = ipv4address.reduce((acc, address) => acc+address[1], 0);
    return result;
}
//console.log(sumofsecondcomponents(data));

function sumoffourcomponents(data) {
    const ipv4address = ipv4addressofpersons(data);
    const result = ipv4address.reduce((acc, address) => acc+address[3], 0);
    return result;
}
//console.log(sumoffourcomponents(data));

function fullname(data) {
    const fullname = data.map((data) => {
        data.full_name = data.first_name +" "+ data.last_name ;
        return data;
    } );
    return fullname;
}
//console.log(fullname(data));

function orgemails(data){
 return data.filter((ele) =>{
    if(ele.email.includes(".org")){
        return ele;
    }
    } );
}
 //console.log(orgemails(data));

 function firstnamesort(data) {
    return data.sort(function(person1,person2){
        if(person1.first_name>person2.first_name)
            return -1;
        else if(person1.first_name<person2.first_name)
            return 1;
        else
            return 0;
    })
 };

//console.log(firstnamesort(data));
    
 





